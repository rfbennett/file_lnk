<#
    .SYNOPSIS
        Helper functions used by various scripts.
    .DESCRIPTION
        These are functions that have been split out for easier maintenance across multiple modules/scripts.
        Ensure any new functions are generic enough to be consumed by multiple Puppet modules if needed.
        Not all Puppet modules will utilize all functions.
        Functions contained within this file should be named in the format of "<verb>-Helper<descriptor>"
    .NOTES
        Origin file may be found here: https://gitlab.com/rfbennett/ps1_extenders
#>

[string]$script:HelperVersion = '2024.07.29'
[string]$script:HelperOutputFile = ''
[boolean]$script:HelperOutputEnabled = $true

Function Write-HelperOutput {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $false)][AllowEmptyString()][string]$Message = '',
        [Parameter(Mandatory = $false)][ValidateSet('Console', 'Debug', 'Warning', 'Error', 'Info')][string]$Type = 'Debug',
        [Parameter(Mandatory = $false)][ValidateSet('Black', 'DarkBlue', 'DarkGreen', 'DarkCyan', 'DarkRed', 'DarkMagenta', 'DarkYellow', 'Gray', 'DarkGray', 'Blue', 'Green', 'Cyan', 'Red', 'Magenta', 'Yellow', 'White')][string]$ForegroundColor = 'DarkGray'
    )
    if ($script:HelperOutputEnabled) {
        if (($Message -ne '') -and (($Type -eq 'Console') -or ($Type -eq 'Error'))) {
            Write-Host "$Message" -ForegroundColor "$ForegroundColor"
        }
    }
    [string]$logType = 'DBG'
    switch ($Type) {
        'Console' { $logType = 'CNS'; break }
        'Warning' { $logType = 'WRN'; break }
        'Error' { $logType = 'ERR'; break }
        'Info' { $logType = 'INF'; break }
    }
    if ('' -ne $script:HelperOutputFile) {
        [boolean]$isExist = Test-Path -Path "$($script:HelperOutputFile)" -ErrorAction 'SilentlyContinue'
        if ($isExist) {
            [string]$user = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
            [string]$nowDate = Get-Date -format "yyyy-MM-dd, HH:mm:ss"
            "$nowDate, $user, $logType, $Message" | Add-Content -Path "$($script:HelperOutputFile)"
        }
        else {
            Write-Warning "Unable to locate the required '$($script:HelperOutputFile)' log file"
        }
    }
}

Function Initialize-HelperOutput {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $false)][string]$Identifier = "puppet_ps_helper",
        [Parameter(Mandatory = $false)][string]$LogFolder = "$($env:WinDir)\Temp",
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $true,
        [Parameter(Mandatory = $false)][Int64]$MaxLogFileSize = 5 #MBs
    )
    if ($LogOutput) {
        [boolean]$isExist = Test-Path -Path "$LogFolder"  -PathType 'Container' -ErrorAction 'SilentlyContinue'
        if (!$isExist) {
            Write-Host "   Creating the '$LogFolder' folder" -ForegroundColor 'DarkGray'
            $null = New-Item -ItemType Directory -Path "$LogFolder"
        }
    }
    [string]$fileName = "$($Identifier)_debug.log"
    $script:HelperOutputFile = "$LogFolder\$fileName"
    $script:HelperOutputEnabled = $LogOutput
    [string]$debugEnvironmentVariable = [System.Environment]::GetEnvironmentVariable('PUPPET_PS_HELPER_DEBUG', [System.EnvironmentVariableTarget]::Machine)
    if ('True' -ne $debugEnvironmentVariable) {
        Write-Host "   To enable debug mode, create a SYSTEM environment variable named 'PUPPET_PS_HELPER_DEBUG' and set the value to 'True" -ForegroundColor 'DarkGray'
        if ($script:HelperOutputEnabled -ne $true) {
            return
        }
    }
    else {
        Write-Host "   Debug mode enabled for PS1 - you may review the contents of '$script:HelperOutputFile'" -ForegroundColor 'DarkGray'
    }
    [string]$nowDate = Get-Date -format "yyyy-MM-dd, HH:mm:ss"
    [boolean]$isExist = Test-Path -Path "$script:HelperOutputFile" -ErrorAction 'SilentlyContinue'
    if ($isExist) {
        [int]$fileSize = [System.Math]::Round(((($script:HelperOutputFile).length) / 1MB), 2)
        if ($fileSize -gt $MaxLogFileSize) {
            Write-Host '   Removing ovesized log file' -ForegroundColor 'DarkGray'
            Remove-Item -Path "$script:HelperOutputFile" -Force -ErrorAction 'SilentlyContinue'
            $isExist = Test-Path -Path "$script:HelperOutputFile" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                Write-Warning "Failed to remove '$script:HelperOutputFile'"
            }
            $null = New-Item -Path "$LogFolder" -Name "$fileName" -ItemType "File" -Value "$nowDate, $($env:UserName), Initialize $Identifier log file`r"
        }
    }
    else {
        $null = New-Item -Path "$LogFolder" -Name "$fileName" -ItemType "File" -Value "$nowDate, $($env:UserName), Initialize $Identifier log file`r"
    }
    Write-HelperOutput -Message "Helpers Version: $($script:HelperVersion)"
    Write-HelperOutput -Message "Helpers PSScriptRoot: $($PSScriptRoot)"
    return
}

Function Set-HelperExtension {
    [OutputType([string])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$FileName,
        [Parameter(Mandatory = $true)][string]$Extension,
        [Parameter(Mandatory = $false)][boolean]$IsExitOnFailure = $true
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      FileName: $FileName"
    Write-HelperOutput -Message "      Extension: $Extension"
    Write-HelperOutput -Message "      IsExitOnFailure: $IsExitOnFailure"
    try {
        [string]$newFileName = $FileName
        if ($FileName -gt 4) {
            [string]$extensionCheck = $FileName.Substring($FileName.Length - 4)
            if ($extensionCheck.ToLower() -ne "$Extension") {
                Write-HelperOutput -Message "      Adding '$Extension' file extension to '$($FileName)'"
                $newFileName = "$($FileName)$($Extension)"
            }
            else {
                Write-HelperOutput -Message '      File extension already set'
            }
        }
        else {
            Write-HelperOutput -Message "      Adding missing '$Extension' to the '$FileName' file name"
            $newFileName = "$($FileName)$($Extension)"
        }
    }
    catch {
        Write-HelperOutput -Message "$($functionName): $_" -Type 'Console' -ForegroundColor 'Red'
        if ($IsExitOnFailure) {
            Invoke-HelperExit -Message "An error occured verifying file extension" -ExitCode 1 -Type 'failure'
        }
        else {
            Write-HelperOutput -Message "      An unexpected error occured verifying file extension" -ForegroundColor 'Red'
        }
    }
    Write-HelperOutput -Message "   $functionName execution complete"
    return $newFileName
}

Function Remove-HelperItem {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$Item,
        [Parameter(Mandatory = $false)][boolean]$IsExitOnFailure = $false
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      Item: $Item"
    Write-HelperOutput -Message "      IsExitOnFailure: $IsExitOnFailure"
    [boolean]$isRemoved = $false
    try {
        [boolean]$isExist = Test-Path -Path "$Item" -ErrorAction 'SilentlyContinue'
        if ($isExist) {
            Write-HelperOutput -Message '      Removing the existing item'
            Remove-Item -Path "$Item" -Force
            $isExist = Test-Path -Path "$Item" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                if ($IsExitOnFailure) {
                    Invoke-HelperExit -Message "Unable to remove existing item" -ExitCode 1 -Type 'failure'
                }
                else {
                    Write-HelperOutput -Message "   Unable to remove existing item" -Type 'Error'
                }
            }
            else {
                $isRemoved = $true
                Write-HelperOutput -Message '      Successfully removed the item' -ForegroundColor 'Green'
            }
        }
        else {
            Write-HelperOutput -Message '      No existing item found'
        }
    }
    catch {
        Write-HelperOutput -Message "      $($functionName): $_" -Type 'Console' -ForegroundColor 'Red'
        if ($IsExitOnFailure) {
            Invoke-HelperExit -Message "An unexpected error occured removing the '$Item' item" -Type 'failure'
        }
        else {
            Write-HelperOutput -Message "   An unexpected error occured removing the '$Item' item" -Type 'Error'
        }
    }
    Write-HelperOutput -Message "   $functionName execution complete"
    return $isRemoved
}


Function Copy-HelperItem {
    [OutputType([Boolean])]
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)][string]$SourceItem,
        [Parameter(Mandatory = $true)][string]$Destination,
        [Parameter(Mandatory = $false)][string]$DestinationItemName = '',
        [Parameter(Mandatory = $false)][boolean]$BackupExistingDestinationItem = $false
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      Destination: $Destination"
    Write-HelperOutput -Message "      SourceItem: $SourceItem"
    Write-HelperOutput -Message "      DestinationItemName: $DestinationItemName"
    Write-HelperOutput -Message "      BackupExistingDestinationItem: $BackupExistingDestinationItem"
    [boolean]$isSuccess = $false
    [boolean]$isExist = Test-Path -Path "$SourceItem" -ErrorAction 'SilentlyContinue'
    if (!$isExist) {
        Write-HelperOutput -Message "   Unable to locate the '$SourceItem' file" -Type 'Warning'
        return $isSuccess
    }
    [string]$sourceItemName = Split-Path -Path "$SourceItem" -Leaf -Resolve -ErrorAction 'SilentlyContinue'
    Write-HelperOutput -Message "      sourceItemName: $sourceItemName"
    [string]$destinationItem = "$Destination\$sourceItemName"
    if ('' -ne "$DestinationItemName") {
        $destinationItem = "$Destination\$DestinationItemName"
    }
    Write-HelperOutput -Message "      destinationItem: $destinationItem"
    if ($BackupExistingDestinationItem) {
        try {
            $isExist = Test-Path -Path "$destinationItem" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                [string]$timestamp = Get-Date -format "MM-dd-yyyy.hh-mm-ss"
                Write-HelperOutput -Message "      Backing up '$destinationItem' as '$destinationItem.$timestamp.bak'"
                Copy-Item -Path "$destinationItem" -Destination "$destinationItem.$timestamp.bak" -Force -ErrorAction 'SilentlyContinue'
                $isExist = Test-Path -Path "$destinationItem.$timestamp.bak" -ErrorAction 'SilentlyContinue'
                if ($isExist) {
                    Write-HelperOutput -Message '      Backup process completed successfully'
                }
                else {
                    Invoke-HelperExit -Message "   Failed to back up '$destinationItem'"
                }
            }
            else {
                Write-HelperOutput -Message "      No existing '$destinationItem' item found to back up"
            }
        }
        catch {
            Write-HelperOutput -Message "      $($functionName): $_" -Type 'Console' -ForegroundColor 'Red'
            Invoke-HelperExit -Message "   An unexpected error occured backing up the existing '$destinationItem' item"
        }
    }
    try {
        [string]$timestamp = Get-Date -format "MM-dd-yyyy.hh-mm-ss"
        Write-HelperOutput -Message "      Copying '$SourceItem' as '$destinationItem'"
        [System.IO.FileInfo]$copiedItem = Copy-Item -Path "$SourceItem" -Destination "$destinationItem" -Force -PassThru -ErrorAction 'SilentlyContinue'
        if ($null -eq $copiedItem) {
            Invoke-HelperExit -Message "   Failed to copy '$SourceItem'"
        }
        else {
            Write-HelperOutput -Message '      Copy process completed successfully'
            $isSuccess = $true
        }
    }
    catch {
        Write-HelperOutput -Message "      $($functionName): $_" -Type 'Console' -ForegroundColor 'Red'
        Invoke-HelperExit -Message "   An unexpected error occured copying the '$SourceItem' item"
    }
    Write-HelperOutput "   $functionName execution complete"
    return $isSuccess
}

Function Set-HelperFolder {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$Folder,
        [Parameter(Mandatory = $false)][boolean]$IsExitOnFailure = $false
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      Folder: $Folder"
    Write-HelperOutput -Message "      IsExitOnFailure: $IsExitOnFailure"
    [boolean]$isCreated = $false
    try {
        [boolean]$isExist = Test-Path -Path "$Folder" -ErrorAction 'SilentlyContinue'
        if ($isExist) {
            $isCreated = $true
            Write-HelperOutput -Message '      Found existing folder'
        }
        else {
            Write-HelperOutput -Message '      Creating folder structure'
            [void](New-Item -ItemType Directory -Path "$Folder")
            $isExist = Test-Path -Path "$Folder" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                $isCreated = $true
                Write-HelperOutput -Message '      Located newly created folder'
            }
            else {
                if ($IsExitOnFailure) {
                    Invoke-HelperExit -Message "Unable to locate the folder" -ExitCode 1 -Type 'failure'
                }
                else {
                    Write-HelperOutput -Message "   Unable to locate the folder" -Type 'Error'
                }
            }
        }
    }
    catch {
        Write-HelperOutput -Message "   $($functionName): $_" -Type 'Console' -ForegroundColor 'Red'
        if ($IsExitOnFailure) {
            Invoke-HelperExit -Message "An unexpected error occured creating the '$Folder' folder" -Type 'failure'
        }
        else {
            Write-HelperOutput -Message "      An unexpected error occured creating the '$Folder' folder" -Type 'Error'
        }
    }
    Write-HelperOutput -Message "   $functionName execution complete"
    return $isCreated
}

Function Test-HelperCompare {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)]$Value,
        [Parameter(Mandatory = $true)]$OtherValue,
        [Parameter(Mandatory = $false)][string]$Identifier = '',
        [Parameter(Mandatory = $false)][boolean]$IsPriorValid = $true,
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $false
    )
    [boolean]$isSame = $true
    if ($LogOutput) {
        if ($Identifier -eq '') {
            Write-HelperOutput -Message "      '$($Value.ToString())' vs '$($OtherValue.ToString())'"
        }
        else {
            Write-HelperOutput -Message "      $($Identifier): '$($Value.ToString())' vs '$($OtherValue.ToString())'"
        }
    }
    if ($IsPriorValid) {
        if ($Value -ne $OtherValue) {
            if ($Identifier -eq '') {
                Write-HelperOutput -Message "The value of '$Value' does not match '$OtherValue'" -ForegroundColor 'Magenta' -Type 'Console'
            }
            else {
                Write-HelperOutput -Message "The value of '$Value' does not match '$OtherValue' ($Identifier)" -ForegroundColor 'Magenta' -Type 'Console'
            }
            $isSame = $false
        }
    }
    else {
        # No need to compare this one, because a prior comparison already failed
        $isSame = $false
    }
    return $isSame
}

Function Invoke-HelperExit {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $false)][string]$Message = '',
        [Parameter(Mandatory = $false)][int]$ExitCode = -1,
        [Parameter(Mandatory = $false)][ValidateSet('success', 'failure')][string]$Type = 'failure'
    )
    switch ($Type) {
        'success' { Write-HelperOutput -Message "$Message" -ForegroundColor 'Green' -Type 'Console' }
        'failure' { Write-HelperOutput -Message "$Message" -ForegroundColor 'Red' -Type 'Error' }
    }
    Write-HelperOutput -Message "================================================="
    Exit $ExitCode
}

Function Test-HelperAdmin {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $false)][boolean]$IsExitOnFailure = $true
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      IsExitOnFailure: $IsExitOnFailure"
    [Security.Principal.WindowsIdentity]$currentProcessToken = [Security.Principal.WindowsIdentity]::GetCurrent()
    [boolean]$isAdmin = [boolean]($currentProcessToken.Groups -contains [Security.Principal.SecurityIdentifier]'S-1-5-32-544')
    if ($isAdmin) {
        Write-HelperOutput -Message "      isAdmin: $isAdmin"
    }
    else {
        if ($IsExitOnFailure) {
            Invoke-HelperExit -Message "isAdmin: $isAdmin" -ExitCode 1 -Type 'failure'
        }
        else {
            Write-HelperOutput -Message "   isAdmin: $isAdmin" -Type 'Error'
        }
    }
    Write-HelperOutput -Message "   $functionName execution complete"
    return $isAdmin
}

Function Initialize-Helper {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$Identifier,
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $true,
        [Parameter(Mandatory = $false)][string]$LogFolder = "$($env:WinDir)\Temp"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Initialize-HelperOutput -Identifier "$Identifier" -LogOutput $LogOutput -LogFolder $LogFolder
    Write-HelperOutput -Message "$functionName executing..."
    Write-HelperOutput -Message "   Identifier: $Identifier"
    Write-HelperOutput -Message "   LogOutput: $LogOutput"
    Write-HelperOutput -Message "   LogFolder: $LogFolder"
    try {
        Write-HelperOutput -Message "   Operating System: $((Get-CimInstance -ClassName CIM_OperatingSystem).Caption)"
        Write-HelperOutput -Message "   OS Service Pack: $((Get-CimInstance -ClassName CIM_OperatingSystem).ServicePackMajorVersion)"
        Write-HelperOutput -Message "   PowerShell Version: $($PSVersionTable.PSVersion)"
        Write-HelperOutput -Message "   PowerShell Edition: $($PSVersionTable.PSEdition)"
        Write-HelperOutput -Message "   Computer Name: $($env:ComputerName)"
        Write-HelperOutput -Message "   User Domain: $($env:UserDomain)"
        Write-HelperOutput -Message "   If appropriate, ensure that the console session was initiated with admin rights ('Run as administrator')" -Type 'Console'
    }
    catch {
        Write-HelperOutput -Message "$_"
        Invoke-HelperExit -Message "An unexpected error occured while initializing" -Type 'failure'
    }
    Write-HelperOutput -Message "$functionName execution complete"
}
