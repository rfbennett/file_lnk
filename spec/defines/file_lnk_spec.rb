require 'spec_helper'

describe 'file_lnk' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with all parameters' do
        let(:title) { 'lnk_shortcut_test01' }
        let(:params) do
          {
            file_name: 'ShortcutLNK01.lnk',
            parent_folder: 'C:\\Users\\Public\\Desktop',
            target_path: 'C:\\Windows\\notepad.exe',
            description: 'This is a test',
            ensure: 'present',
            hot_key: 'ALT+CTRL+2',
            icon_index: 0,
            icon_file: 'C:\\Windows\\HelpPane.exe',
            log_output: true,
            set_run_as_administrator: false,
            target_arguments: '"C:\\Windows\\system.ini"',
            window_style: '7',
            working_directory: 'C:\\Windows',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_exec("Set 'ShortcutLNK01.lnk' file_lnk shortcut") }
        it { is_expected.to contain_file('C:/ProgramData/Puppet/file_lnk').with('ensure' => 'absent') }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - description: 'This is a test'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - ensure: 'present'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - hot_key: 'ALT+CTRL+2'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - icon: 'C:\\Windows\\HelpPane.exe'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - icon_file: 'C:\\Windows\\HelpPane.exe'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - icon_index: '0'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - parent_folder: 'C:\\Users\\Public\\Desktop'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - set_run_as_administrator: 'false'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - target_path: 'C:\\Windows\\notepad.exe'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - window_style: '7'") }
        it { is_expected.to contain_notify("file_lnk - ShortcutLNK01.lnk - working_directory: 'C:\\Windows'") }
      end

      context 'with missing file_name parameter' do
        let(:title) { 'lnk_shortcut_test02' }
        let(:params) do
          {
            parent_folder: 'C:\\Users\\Public\\Desktop',
            target_path: 'C:\\Windows\\write.exe',
          }
        end

        it { is_expected.to compile.and_raise_error(%r{expects a value for parameter 'file_name'}) }
      end

      context 'with ensure absent' do
        let(:title) { 'lnk_shortcut_test03' }
        let(:params) do
          {
            file_name: 'ShortcutLNK03.lnk',
            parent_folder: 'C:\\Users\\Public\\Desktop',
            ensure: 'absent',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_tidy("Remove 'ShortcutLNK03.lnk' file_lnk shortcut") }
      end
    end
  end
end
