# File LNK

Puppet defined type for managing .LNK files within Microsoft Windows.

## Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Development - Guide for contributing to the module](#development)

## Description

This module supplies a defined type that can be used to manage .LNK shortcut files on a Windows node. It leverages PowerShell to do the bulk of the heavy lifting.

## Usage

To use the file_lnk type, you must (at a minimum) specify file_name, parent_folder and target_path (as shown below). Note that, where possible, the shortcut creation process will try to automatically generate missing parent folders.

You can make a single shortcut:
```
  file_lnk { 'JustOneShortcut':
    file_name     => 'SingleShortcutExample.lnk',
    parent_folder => 'C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\AFolder',
    target_path   => 'C:\\Windows\\write.exe',
  }
```

Or you can loop through an array (of hashes) of shortcuts and do many at once (note that a Title is not required within the Array as it gets added when supplying the data to the defined resource type):
```
class just_an_example_class (
  Array[Hash] $shortcuts_lnk = [
    {
      file_name     => 'BasicShortcutExample.lnk',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      target_path   => 'C:\\Windows\\write.exe',
    },
    {
      file_name        => 'AnotherSimpleShortcutExample.lnk',
      parent_folder    => 'C:\\Users\\Public\\Desktop',
      target_path      => 'C:\\Windows\\write.exe',
      target_arguments => '"C:\\Windows\\system.ini"',
      window_style     => '3',
      description      => 'This example opens a file in maximized mode',
      log_output       => true,
    },
    {
      file_name                => 'YetAnotherShortcutExample.lnk',
      parent_folder            => 'C:\\Users\\Public\\Desktop',
      target_path              => 'C:\\Windows\\write.exe',
      hot_key                  => '',
      icon_file                => 'C:\\Windows\\winhlp32.exe',
      icon_index               => 0,
      description              => 'Will run with admin rights',
      set_run_as_administrator => true,
      log_output               => true,
    },
    {
      file_name         => 'OneMoreShortcutExample.lnk',
      parent_folder     => 'C:\\Users\\Public\\Desktop',
      target_path       => 'C:\\Program Files\\Internet Explorer\\iexplore.exe',
      target_arguments  => '"https://puppet.com"',
      hot_key           => 'CTRL+ALT+8',
      icon_file         => 'C:\\Folder1\\puppet.ico',
      icon_index        => 0,
      working_directory => 'C:\\Program Files',
      log_output        => true,
    },
    {
      file_name     => 'OneOtherShortcutExample.lnk',
      parent_folder => 'C:\\Users\\Public\\Desktop',
      target_path   => 'C:\\Windows\\System32',
      log_output    => true,
    }
  ],
) {
  $shortcuts_lnk.each | String Hash $shortcuts_lnk_hash | {
    file_lnk { $shortcuts_lnk_hash['file_name']:
      * => $shortcuts_lnk_hash
    }
  }
}
```
You can also remove a shortcut:
```
  file_lnk { 'ShortcutCleanup':
    ensure        => 'absent',
    file_name     => 'UnwantedShortcut.lnk',
    parent_folder => 'C:\\Users\\Public\\Desktop',
  }
```

## Limitations

1. This modules uses the following to generate the contents of the REFERENCE.md file:
  * `puppet strings generate --format markdown --out REFERENCE.md`
2. This module has been evaluated against the following:
  * Microsoft Windows Server 2022 (running PowerShell 5.1.20348.643)
  * Microsoft Windows Server 2019 (running PowerShell 5.1.17763.1971)
  * Microsoft Windows Server 2016 (running PowerShell 5.1.14393.4530)
  * Microsoft Windows 10 (running PowerShell 5.1.19041.1023)

## Development

Feedback and ideas are always welcome - please contact an Author (listed in metadata.json) to discuss your input, or feel free to simply [open an Issue](https://gitlab.com/rfbennett/file_lnk/-/issues).
