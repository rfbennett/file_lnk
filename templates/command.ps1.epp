<#
    .SYNOPSIS
        Manage .LNK shortcut files within Microsoft Windows.
    .DESCRIPTION
        Updates/Creates .LNK shortcut files.
        Should be run with administrative rights in case destination location requires elevated privileges.
        We use the Create/Copy methodology in order to try and get File Explorer to update itself with recent changes.
#>

Function Set-FileLNK {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$File,
        [Parameter(Mandatory = $true)][string]$TargetPath,
        [Parameter(Mandatory = $false)][string]$Arguments = '',
        [Parameter(Mandatory = $false)][string]$Description = '',
        [Parameter(Mandatory = $false)][string]$HotKey = '',
        [Parameter(Mandatory = $false)][string]$IconFile = "$TargetPath",
        [Parameter(Mandatory = $false)][int]$IconIndex = 0,
        [Parameter(Mandatory = $false)][boolean]$IsRunAsAdministrator = $false,
        [Parameter(Mandatory = $false)][int]$WindowStyle = 1,
        [Parameter(Mandatory = $false)][string]$WorkingDirectory = "$($env:WinDir)"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "   $functionName executing..."
    Write-HelperOutput -Message "      File: $File"
    Write-HelperOutput -Message "      TargetPath: $TargetPath"
    Write-HelperOutput -Message "      Arguments: $Arguments"
    Write-HelperOutput -Message "      Description: $Description"
    Write-HelperOutput -Message "      HotKey: $HotKey"
    Write-HelperOutput -Message "      IconFile: $IconFile"
    Write-HelperOutput -Message "      IconIndex: $IconIndex"
    Write-HelperOutput -Message "      IsRunAsAdministrator: $IsRunAsAdministrator"
    Write-HelperOutput -Message "      WindowStyle: $WindowStyle"
    Write-HelperOutput -Message "      WorkingDirectory: $WorkingDirectory"
    [boolean]$isExist = Test-Path -Path "$File" -ErrorAction 'SilentlyContinue'
    if ($isExist) {
        Write-HelperOutput -Message '      Existing .LNK file found (previously triggered functions should have removed it)' -ForegroundColor 'Yellow'
    }
    if (($IconFile.StartsWith('"')) -or ($IconFile.EndsWith('"'))) {
        Write-HelperOutput -Message "      Adjusted IconFile: $($IconFile.Trim('"'))"
    }
    $IconFile = $IconFile.Trim('"')
    [System.__ComObject]$shell = New-Object -ComObject WScript.Shell 
    [System.__ComObject]$shortcut = $shell.CreateShortcut($File)
    $shortcut.TargetPath = "$TargetPath"
    $shortcut.Arguments = "$Arguments"
    $shortcut.Description = "$Description"
    $shortcut.IconLocation = "$IconFile,$IconIndex"
    $shortcut.WindowStyle = $WindowStyle
    $shortcut.HotKey = $HotKey # will only work if resides in certain locations (such as Desktop, Start Menu, etc..)
    $shortcut.WorkingDirectory = "$WorkingDirectory"
    $shortcut.Save()
    Write-HelperOutput -Message '      Releasing resources'
    while ( [System.Runtime.Interopservices.Marshal]::ReleaseComObject($shortcut)) {}
    while ( [System.Runtime.Interopservices.Marshal]::ReleaseComObject($shell)) {}
    [System.GC]::Collect()
    [System.GC]::WaitForPendingFinalizers()
    [boolean]$isExist = Test-Path -Path "$File" -ErrorAction 'SilentlyContinue'
    if ($isExist) {
        if ($IsRunAsAdministrator) {
            Write-HelperOutput -Message "      Setting newly created temp shortcut to 'Run as Administrator'"
            [System.Byte[]]$bytes = [System.IO.File]::ReadAllBytes($File)
            $bytes[0x15] = $bytes[0x15] -bor 0x20 # set byte 21 (0x15) bit 6 (0x20) ON
            $bytes | Set-Content $File -Encoding Byte
        }
        Write-HelperOutput -Message "      Successfully created the temp shortcut file" -ForegroundColor 'Green'
    }
    else {
        Invoke-HelperExit -Message "Failed to create the temp shortcut file" -ExitCode 1 -Type 'failure'
    }
    Write-HelperOutput -Message "   $functionName execution complete"
}

Function Initialize-FileLNK {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$FileName,
        [Parameter(Mandatory = $true)][string]$ParentFolder,
        [Parameter(Mandatory = $true)][string]$TargetPath,
        [Parameter(Mandatory = $false)][string]$Arguments = '',
        [Parameter(Mandatory = $false)][string]$Description = '',
        [Parameter(Mandatory = $false)][string]$HotKey = '',
        [Parameter(Mandatory = $false)][string]$IconFile = "$TargetPath",
        [Parameter(Mandatory = $false)][int]$IconIndex = 0,
        [Parameter(Mandatory = $false)][boolean]$IsRunAsAdministrator = $false,
        [Parameter(Mandatory = $false)][int]$WindowStyle = 1,
        [Parameter(Mandatory = $false)][string]$WorkingDirectory = "$($env:WinDir)"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Write-HelperOutput -Message "$functionName executing..."
    Write-HelperOutput -Message "   FileName: $FileName"
    Write-HelperOutput -Message "   ParentFolder: $ParentFolder"
    Write-HelperOutput -Message "   TargetPath: $TargetPath"
    Write-HelperOutput -Message "   Arguments: $Arguments"
    Write-HelperOutput -Message "   Description: $Description"
    Write-HelperOutput -Message "   HotKey: $HotKey"
    Write-HelperOutput -Message "   IconFile: $IconFile"
    Write-HelperOutput -Message "   IconIndex: $IconIndex"
    Write-HelperOutput -Message "   IsRunAsAdministrator: $IsRunAsAdministrator"
    Write-HelperOutput -Message "   WindowStyle: $WindowStyle"
    Write-HelperOutput -Message "   WorkingDirectory: $WorkingDirectory"
    $null = Test-HelperAdmin -IsExitOnFailure $true
    [string]$lastCharacter = $ParentFolder.Substring($ParentFolder.Length - 1)
    if ($lastCharacter -eq "\") {
        Write-HelperOutput -Message '   Removing trailing slash'
        $ParentFolder = $ParentFolder.Substring(0, ($ParentFolder.Length - 1))
        Write-HelperOutput -Message "   Adjusted ParentFolder: $ParentFolder"
    }
    # Make sure there are not multiple trailing slashes
    $lastCharacter = $ParentFolder.Substring($ParentFolder.Length - 1)
    if ($lastCharacter -eq "\") {
        Write-HelperOutput -Message '   Removing second trailing slash'
        $ParentFolder = $ParentFolder.Substring(0, ($ParentFolder.Length - 1))
        Write-HelperOutput -Message "   Adjusted ParentFolder: $ParentFolder"
    }
    [string]$adjustedFileName = Set-HelperExtension -FileName "$FileName" -Extension ".lnk"
    Write-HelperOutput -Message "   adjustedFileName: $adjustedFileName"
    $null = Set-HelperFolder -Folder "$ParentFolder" -IsExitOnFailure $true
    $null = Remove-HelperItem -Item "$ParentFolder\$adjustedFileName" -IsExitOnFailure $true
    # We create, and then copy, the shortcut so that File Explorer reflects any new changes
    Set-FileLNK -TargetPath $TargetPath `
        -File "$($env:WinDir)\Temp\$adjustedFileName" `
        -Arguments $Arguments `
        -Description $Description `
        -HotKey $HotKey `
        -IconFile $IconFile `
        -IconIndex $IconIndex `
        -IsRunAsAdministrator $IsRunAsAdministrator `
        -WindowStyle $WindowStyle `
        -WorkingDirectory $WorkingDirectory
    Write-HelperOutput -Message "   Copying temp shortcut to final desintation"
    $null = Copy-HelperItem -SourceItem "$($env:WinDir)\Temp\$adjustedFileName" -Destination "$ParentFolder" -DestinationItemName "$adjustedFileName"
    Write-HelperOutput -Message "   Cleaning up temp shortcut"
    $null = Remove-HelperItem -Item "$($env:WinDir)\Temp\$adjustedFileName" -IsExitOnFailure $true
    Write-HelperOutput -Message "$functionName execution complete"
}



[boolean]$logOutput = $true
if (!'<%= $identifier %>'.StartsWith('<')) {
    # This script was triggered by Puppet processes, and should be the only section to consume Puppet-based variables
    if (('<%= $log_output %>' -eq '') -or ('<%= $log_output %>' -eq 'false')) {
        $logOutput = $false
    }
    Initialize-Helper -Identifier '<%= $identifier %>' -LogOutput $logOutput -LogFolder "$($env:WinDir)\Temp"
    [boolean]$isRunAsAdministrator = ([System.Convert]::ToBoolean('<%= $set_run_as_administrator %>'))
    Initialize-FileLNK -FileName '<%= $file_name %>' `
        -ParentFolder '<%= $parent_folder %>' `
        -TargetPath '<%= $target_path %>' `
        -Arguments '<%= $target_arguments %>' `
        -Description '<%= $description %>' `
        -HotKey '<%= $hot_key %>' `
        -IconFile '<%= $icon_file %>' `
        -IconIndex '<%= $icon_index %>' `
        -IsRunAsAdministrator $isRunAsAdministrator `
        -WindowStyle '<%= $window_style %>' `
        -WorkingDirectory '<%= $working_directory %>'
}
else {
    . "$(Split-Path -Path "$PSScriptRoot" -Parent)\files\helpers.ps1"
    Initialize-Helper -Identifier "file_lnk" -LogOutput $logOutput -LogFolder "$($env:WinDir)\Temp"
    Initialize-FileLNK -FileName "DefaultExampleLnkShortcut01.lnk" `
        -ParentFolder "C:\Users\Public\Desktop" `
        -TargetPath "C:\Windows\write.exe" `
        -Arguments """C:\Windows\system.ini""" `
        -Description "This is an example" `
        -HotKey "CTRL+ALT+7" `
        -IconFile "C:\Windows\write.exe" `
        -IconIndex 0 `
        -IsRunAsAdministrator $false `
        -WindowStyle 3 `
        -WorkingDirectory "$($env:WinDir)"
    # Initialize-FileLNK -FileName "DefaultExampleLnkShortcut02" `
    #     -ParentFolder "C:\Users\Public\Desktop" `
    #     -TargetPath """C:\Program Files\Windows Media Player\wmplayer.exe""" `
    #     -Arguments """C:\Windows\Media\chimes.wav""" `
    #     -Description "This is another example" `
    #     -HotKey "CTRL+ALT+8" `
    #     -IconFile """C:\Program Files\Windows Media Player\wmplayer.exe""" `
    #     -IconIndex 0 `
    #     -IsRunAsAdministrator $false `
    #     -WindowStyle 3 `
    #     -WorkingDirectory "$($env:ProgramFiles)\Windows Media Player"
    # Initialize-FileLNK -FileName "DefaultExampleLnkShortcut03" `
    #     -ParentFolder "C:\Users\Public\Desktop" `
    #     -TargetPath "C:\Windows\notepad.exe" `
    #     -Arguments "" `
    #     -Description "This is yet another example" `
    #     -HotKey "CTRL+ALT+9" `
    #     -IconFile 'C:\Windows\system32\SHELL32.dll' `
    #     -IconIndex 2 `
    #     -IsRunAsAdministrator $false `
    #     -WindowStyle 1 `
    #     -WorkingDirectory "C:\Windows"
}
Invoke-HelperExit -Message "Exiting with a 0" -ExitCode 0 -Type 'success'
