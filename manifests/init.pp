# @summary File LNK
#
# Defines a .LNK shortcut
#
# @param description
#   Descriptive comment to use for the shortcut
#   Example: 'This is an example comment'
#
# @param ensure
#   Desired state of the shortcut
#   Example: 'absent'
#
# @param file_name
#   Assigned text, including the file extension, that should be used as the name of the file
#      Ensure that the exact file name is used if 'ensure' is set to 'absent'
#   Example: 'OneToRuleThemAll.lnk'
#
# @param hot_key
#   Denotes the key combination to assign to the shortcut
#   Key combo execution will only work if the shortcut resides in certain locations (such as Desktop, Start Menu, etc..)
#   Example: 'CTRL+ALT+8'
#
# @param icon_file
#   Full path to the file containing the icon to use for the shortcut
#   Example: 'C:\Windows\HelpPane.exe'
#
# @param icon_index
#   Denotes which icon entry within the icon_file item to use for the shortcut
#   Example: 0
#
# @param log_output
#   Determines additional logging output (typically only used for troubleshooting) during a Puppet agent run
#   Example: True
#
# @param parent_folder
#   Full path to the parent folder that will house the shortcut file
#   Example: 'C:\Users\Public\Desktop'
#
# @param set_run_as_administrator
#   Determines if the shortcut should be run with administrator rights
#   Example: True
#
# @param target_path
#   Path to the item to launch when the shortcut is triggered
#      Required when 'ensure' is set to 'present'
#   Example: 'C:\\Windows\\notepad.exe'
#
# @param target_arguments
#   Arguments to supply to the target file when the shorcut is triggered
#   Example: '-option123'
#
# @param window_style
#   Denotes what type of window to use when the shortcut is triggered
#   1=normal, 3=maximized, 7=minimized
#   Example: '3'
#
# @param working_directory
#   Path to use for the working directory when the shortcut is triggered
#   Example: 'C:\\Windows'
#
define file_lnk (
  String $file_name,
  String $parent_folder,
  Optional[String] $description = undef,
  Enum['present', 'absent'] $ensure = 'present',
  Optional[String] $hot_key = undef,
  Optional[String] $icon_file = undef,
  Optional[Integer] $icon_index = undef,
  Boolean $log_output = false,
  Boolean $set_run_as_administrator = false,
  Optional[String] $target_arguments = undef,
  Optional[String] $target_path = undef,
  Enum['1', '3', '7'] $window_style = '1',
  String $working_directory = 'C:\\Windows',
) {
  if ($log_output) {
    notify { "${module_name} - ${file_name} - ensure: '${ensure}'": }
    notify { "${module_name} - ${file_name} - parent_folder: '${parent_folder}'": }
  }
  # Cleanup of unwanted folder structure from prior versions (this will be removed in a subsequent version)
  if ! defined(File["C:/ProgramData/Puppet/${module_name}"]) {
    file { "C:/ProgramData/Puppet/${module_name}" :
      ensure  => absent,
      recurse => true,
      force   => true,
    }
  }
  if ($ensure == 'absent') {
    tidy { "Remove '${file_name}' ${module_name} shortcut":
      path    => $parent_folder,
      age     => '0',
      recurse => 1,
      matches => [$file_name],
    }
  } else {
    if ($icon_file == undef) {
      $icon = $target_path
    } else {
      $icon = $icon_file
    }
    if ($log_output) {
      notify { "${module_name} - ${file_name} - description: '${description}'": }
      notify { "${module_name} - ${file_name} - hot_key: '${hot_key}'": }
      notify { "${module_name} - ${file_name} - icon: '${icon}'": }
      notify { "${module_name} - ${file_name} - icon_file: '${icon_file}'": }
      notify { "${module_name} - ${file_name} - icon_index: '${icon_index}'": }
      notify { "${module_name} - ${file_name} - set_run_as_administrator: '${set_run_as_administrator}'": }
      notify { "${module_name} - ${file_name} - target_path: '${target_path}'": }
      notify { "${module_name} - ${file_name} - window_style: '${window_style}'": }
      notify { "${module_name} - ${file_name} - working_directory: '${working_directory}'": }
    }
    if ($target_path == undef) or ($target_path == '') {
      fail "Invalid/Empty 'target_path' supplied for '${file_name}'"
    }
    $epp_param_hash = {
      'description'              => $description,
      'file_name'                => $file_name,
      'hot_key'                  => $hot_key,
      'identifier'               => $module_name,
      'icon_file'                => $icon,
      'icon_index'               => $icon_index,
      'log_output'               => $log_output,
      'parent_folder'            => $parent_folder,
      'set_run_as_administrator' => $set_run_as_administrator,
      'target_arguments'         => $target_arguments,
      'target_path'              => $target_path,
      'window_style'             => $window_style,
      'working_directory'        => $working_directory,
    }
    exec { "Set '${file_name}' ${module_name} shortcut":
      provider  => powershell,
      command   => "${file("${module_name}/helpers.ps1")};
                    ${epp("${module_name}/command.ps1.epp", $epp_param_hash)}",
      onlyif    => "${file("${module_name}/helpers.ps1")};
                    ${epp("${module_name}/onlyif.ps1.epp", $epp_param_hash)}",
      logoutput => $log_output,
    }
  }
}
